<?php
namespace jeffreycwitt\Composer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class LombardPressInstaller extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        return 'lombardpress/';
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'jeffreycwitt-lombardpress' === $packageType;
    }
}

?>